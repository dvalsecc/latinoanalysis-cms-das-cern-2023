#!/usr/bin/env python

from os import environ
baseDir  = environ["LATINOS_BASEDIR"] 
jobDir   = baseDir+'jobs/'
workDir  = baseDir+'workspace/'
jobDirSplit = True
singularityImage = "/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/latinoanalysis-cms-das-cern-2023:lxplus-cc7-latest"

