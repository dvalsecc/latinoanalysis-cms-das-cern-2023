#!/bin/bash

# exit when any command fails; be verbose
set -e
pwd
# make cmsrel etc. work
shopt -s expand_aliases

export MY_BUILD_DIR=${PWD}


## CONDOR SUBMISSION
# Now setting up the things needed for condor submission. 
# Copying from https://gitlab.cern.ch/batch-team/dask-lxplus/-/blob/master/Dockerfile.lxdask-cc7 
cd docker 
cp Miniconda3-latest-Linux-x86_64.sh /tmp/miniconda.sh
cp batch7-stable.repo /etc/yum.repos.d/
yum -y update \
    && yum -y install curl which bzip2 libgfortran git file man-db gcc-c++ vim nano myschedd krb5-workstation ngbauth-submit  \
    binutils libX11-devel libXpm-devel libXft-devel libXext-devel python openssl-devel \
    && bash /tmp/miniconda.sh -bfp /usr/local/ \
    && rm -f /tmp/miniconda.sh \
    && conda update conda \
    && conda clean --all --yes \
    && yum clean all


conda install --yes --freeze-installed \
    -c conda-forge \
    voms \
    xrootd==5.3.0 \
    ca-policy-lcg \
    htcondor \
    && conda clean --all -f -y
  #  && conda build purge-all


# note that we are using the conda condor
mkdir -p /usr/local/etc/condor/config.d
cp condor_submit.config /usr/local/etc/condor/config.d/

# but config still looked for in /etc/condor for some reason
mkdir -p /etc/condor/
cp condor_config /etc/condor/
mkdir -p /etc/condor/config.d
cp condor_submit.config /etc/condor/config.d/

cp ngauth_batch_crypt_pub.pem /etc/
cp ngbauth-submit /etc/sysconfig/
cp myschedd.yaml /etc/myschedd/
# we need a functional krb5.conf and this one comes from ngbauth-submit
rm -f /etc/krb5.conf
ln -s /etc/krb5.conf.no_rdns /etc/krb5.conf


cd $MY_BUILD_DIR
####################3
#Now building the latinos stuff

echo "Installing CMSSW and Latinos" 
export SCRAM_ARCH=slc7_amd64_gcc820

source /cvmfs/cms.cern.ch/cmsset_default.sh
cd /opt

cmsrel CMSSW_10_6_27
cd CMSSW_10_6_27/src/
cmsenv


git clone https://github.com/latinos/LatinoAnalysis.git LatinoAnalysis
cd LatinoAnalysis
git checkout DAS_2023
git rev-parse HEAD

cp ${MY_BUILD_DIR}/userConfig.py Tools/python/

echo " - MultiDraw Plotting Tools"
git clone https://github.com/yiiyama/multidraw.git MultiDraw
cd MultiDraw
git checkout 2.0.12 2>/dev/null
./mkLinkDef.py --cmssw
cd ../..

echo "Compile everything"
scramv1 b -j  
chmod -R +777 ./

############################################
# Installing combine
echo "Installing combine"
cd /opt
cmsrel CMSSW_10_2_13
cd CMSSW_10_2_13/src/
cmsenv

git clone https://github.com/cms-analysis/CombineHarvester.git CombineHarvester
git clone https://github.com/cms-analysis/HiggsAnalysis-CombinedLimit.git HiggsAnalysis/CombinedLimit
cd HiggsAnalysis/CombinedLimit
cd $CMSSW_BASE/src/HiggsAnalysis/CombinedLimit
git fetch origin
git checkout v8.1.0
scramv1 b clean; scramv1 b -j


# Utility scripts

echo "Installing utils scripts"

echo "source /cvmfs/cms.cern.ch/cmsset_default.sh && cd /opt/CMSSW_10_6_27/src && cmsenv && export DISPLAY=:0 && cd -;" > /usr/bin/activate-latinos;
echo "source /cvmfs/cms.cern.ch/cmsset_default.sh && cd /opt/CMSSW_10_2_13/src && cmsenv && export DISPLAY=:0 && cd -;" > /usr/bin/activate-combine;
chmod +x /usr/bin/activate-latinos
chmod +x /usr/bin/activate-combine

chmod +x ${MY_BUILD_DIR}/scripts/*
cp -r ${MY_BUILD_DIR}/scripts/* /usr/bin

