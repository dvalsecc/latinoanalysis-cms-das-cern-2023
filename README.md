This is a repository to build a docker/singularity image for the Latinos framework. 

The goal is to simplify the installation for the CMS-DAS-2023 HWW long exercise.

- [x] Apptainer unpacked image
- [x] Change Latino code to run on condor with apptainer
- [x] Tested condor submission
